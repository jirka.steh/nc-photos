// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'files_controller.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$FilesControllerNpLog on FilesController {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("controller.files_controller.FilesController");
}

// **************************************************************************
// ToStringGenerator
// **************************************************************************

extension _$UpdatePropertyFailureErrorToString on UpdatePropertyFailureError {
  String _$toString() {
    // ignore: unnecessary_string_interpolations
    return "UpdatePropertyFailureError {fileIds: [length: ${fileIds.length}]}";
  }
}

extension _$RemoveFailureErrorToString on RemoveFailureError {
  String _$toString() {
    // ignore: unnecessary_string_interpolations
    return "RemoveFailureError {fileIds: [length: ${fileIds.length}]}";
  }
}
